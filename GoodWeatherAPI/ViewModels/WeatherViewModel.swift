import SwiftUI
import Combine

// PROVIDES ALL THE DATA TO THE VIEW, SO IT CAN BE DISPLAYED
class WeatherViewModel: ObservableObject {
	// MARK: - ©PROPERTIES
	var cityName: String = ""
	private var weatherService: WeatherService
	
	/// Notifies all subcribers that the weather object
	/// instance has changed. Similar to a didSet computed
	/// property. Once the singleton instance changes,
	/// it then publishes the event change
	@Published var weather = Weather()
	
	// MARK: - COMPUTED-PROPERTY
	var temperature: String {
		if let temp = weather.temp {
			return String(format: "%.0f", temp)
		} else { return "" }
	}
	
	var humidity: String {
		if let humidity = weather.humidity {
			return String(format: "%.0f", humidity)
		} else { return "" }
	}
	
	// MARK: - ©init
	/**©-------------------------------------------©*/
	init() {
		weatherService = WeatherService()
	}
	/**©-------------------------------------------©*/
	
	// MARK: - Class methods
	internal func fetchWeather(with city: String) {
		///|__________|
		weatherService.fectchWeatherWith(city: city) { [weak self] result in
			///|__________|
			DispatchQueue.main.async {
				// Passing our weather data
				switch result {
					case .success(let weather):
						printf(weather)
						self?.weather = weather
					case .failure(let error): printf(error.localizedDescription)
				}
			}
		}
	}///:END__>
	
	internal func searchWith() {
		if let city = cityName
			.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
			fetchWeather(with: city)
		}
	}///:END__>
}// END: [CLASS]
