import SwiftUI

struct ContentView: View {
	// MARK: - ©PROPERTIES
	
	// MARK: - ©State-->PROPERTIES
	@ObservedObject var weatherVM: WeatherViewModel
	
	// MARK: - ©init
	/**©-------------------------------------------©*/
	init() { weatherVM = WeatherViewModel() }
	/**©-------------------------------------------©*/
	
	/*©-----------------------------------------©*/
	var body: some View {
		
		VStack(alignment: .center) {
			
			TextField("Enter city name",
					  text: $weatherVM.cityName) {
				///|__________|
				weatherVM.searchWith()
				///|     ⇪     |
			}.font(.custom("Arial", size: 40)).padding()
			.padding(.leading, 30).padding(.trailing, 30)
			.fixedSize().background(Color.black).cornerRadius(15)
			
			// Actually display the weather
			Text("\(weatherVM.temperature)•F").font(.custom("Arial", size: 80))
				.foregroundColor(.white)
				.offset(y: 100).padding()
			
		}///:END__>VSTACK
		.frame(minWidth: 0, maxWidth: .infinity,
			   minHeight: 0,maxHeight: .infinity)
		.background(Color.gray).edgesIgnoringSafeArea(.all)
		/*÷÷÷÷÷÷÷÷÷÷÷÷*/
		
	}
	/*©-----------------------------------------©*/
	
	// MARK: - Helper methods
	
	
}// END: [STRUCT]

struct ContentView_Previews: PreviewProvider {
	
	static var previews: some View {
		
		ContentView()
			.preferredColorScheme(.dark)
	}
}
