import SwiftUI

class WeatherService {
	// MARK: - ©PROPERTIES
	let apiKey = "36e35aade87f6f4aaf1d09eea048d46f"
	// https://api.openweathermap.org/data/2.5/weather?q=Houston&appid=36e35aade87f6f4aaf1d09eea048d46f&units=imperial
    // MARK: - ©READ|FETCH
    func fectchWeatherWith(city: String, completion: @escaping (Result<Weather, Error>) -> Void) {
        ///|__________|
        guard let url = URL(string: "https://api.openweathermap.org/data/2.5/" +
								"weather?q=\(city)" +
								"&appid=\(apiKey)" +
								"&units=imperial") else { return }
        
        // URLSession
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            // Handling error
            if let error = error {
                printf("Could not fetch data..\n \(error.localizedDescription)")
				return completion(.failure(error))
            }
            
            // Handling the data is not equal to nil
			guard let data = data else {
				return completion(.failure(error?.localizedDescription as! Error))
			}
            
            // DECODER INSTANCE
            let d = JSONDecoder()
            
			do {
				let weatherResponse = try d.decode(WeatherResponse.self, from: data)
				
				// Completes success with a weather object
				let weather = weatherResponse.main
				completion(.success(weather))
			} catch {
				printf("Could not fetch data..\n \(error)")
				completion(.failure(error))
			}
        }.resume()
    }
}// END: [CLASS]
