import Foundation

// MARK: -#Root structure {}
struct WeatherResponse: Codable {
    // MARK: - ©PROPERTIES
    let main: Weather
}

struct Weather: Codable {
    // MARK: - ©PROPERTIES
    var temp, humidity: Double?
}


